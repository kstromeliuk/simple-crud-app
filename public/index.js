import 'https://cdnjs.cloudflare.com/ajax/libs/framework7/5.7.10/js/framework7.bundle.js';
// import "https://cdnjs.cloudflare.com/ajax/libs/firebase/7.16.0/firebase-app.min.js";
// import "https://cdnjs.cloudflare.com/ajax/libs/firebase/7.16.0/firebase-database.min.js";
// import { getAuth, signInWithPopup, GoogleAuthProvider } from "https://cdnjs.cloudflare.com/ajax/libs/firebase/7.16.1/firebase-auth.min.js";
import { initializeApp } from 'https://www.gstatic.com/firebasejs/9.6.10/firebase-app.js'
import { getAuth, signInWithPopup, GoogleAuthProvider, createUserWithEmailAndPassword, signInWithEmailAndPassword, signOut, onAuthStateChanged } from 'https://www.gstatic.com/firebasejs/9.6.10/firebase-auth.js'
// import 'https://www.gstatic.com/firebasejs/9.6.10/firebase-database.js'
import {firebaseConfig} from "./firebase.js";
import app from "./F7App.js";
import "./cookbook.js";

initializeApp(firebaseConfig);
const $$ = Dom7;
window.app = app
const auth = getAuth()

onAuthStateChanged(auth, (user) => {
    if (user) {
        app.tab.show("#tab2", true);
        $$('#infoBeforeLogin').css('display', 'none')
    } else {
        app.tab.show("#tab1", true);
    }
});

$$("#signInButton").on("click", (evt) => {
    evt.preventDefault();
    var formData = app.form.convertToData('#loginForm');
    signInWithEmailAndPassword(auth, formData.username, formData.password).then(
        () => {
            // could save extra info in a profile here I think.
            app.loginScreen.close(".loginYes", true);
        }
    ).catch(function(error) {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        $$("#signInError").html(errorCode + " error " + errorMessage)
        console.log(errorCode + " error " + errorMessage);
        // ...
    });

});

$$("#signUpButton").on("click", (evt) => {
    evt.preventDefault();
    var formData = app.form.convertToData('#signUpForm');
    //alert("clicked Sign Up: " + JSON.stringify(formData));
    createUserWithEmailAndPassword(auth, formData.username, formData.password).then(
        () => {
            // could save extra info in a profile here I think.
            app.loginScreen.close(".signupYes", true);
        }
    ).catch((error) => {
        // Handle Errors here.
        var errorCode = error.code;
        var errorMessage = error.message;
        $$("#signUpError").html(errorCode + " error " + errorMessage)
        console.log(errorCode + " error " + errorMessage);
        // ...
    });

});

$$('.googleOAuth').on('click', (evt) => {
    evt.preventDefault()

    signInWithPopup(auth, new GoogleAuthProvider())
        .then(() => {
            app.loginScreen.close(".loginYes", true);
            app.loginScreen.close(".signupYes", true);
        }).catch((error) => {
        const errorCode = error.code;
        const errorMessage = error.message;

        $$("#signUpError").html(errorCode + " error " + errorMessage)
        console.log(errorCode + " error " + errorMessage);
    });

})

$$("#logout").on("click", () => {
    signOut(auth).then(() => {
        $$('#infoBeforeLogin').css('display', 'block')
        // Sign-out successful.
    }).catch(() => {
        // An error happened.
    });
});
