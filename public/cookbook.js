import {initializeApp} from "https://www.gstatic.com/firebasejs/9.6.10/firebase-app.js";
import {getAuth} from "https://www.gstatic.com/firebasejs/9.6.10/firebase-auth.js";
import {getDatabase, child, ref, set, get, onValue, update, remove} from "https://www.gstatic.com/firebasejs/9.6.10/firebase-database.js";
import {DateTime, Settings} from 'https://cdn.jsdelivr.net/npm/luxon@2.0.2/build/es6/luxon.js'
import app from "./F7App.js";
import {firebaseConfig} from "./firebase.js";

const $$ = Dom7;

Settings.defaultLocale = "en";

initializeApp(firebaseConfig);

const db = getDatabase()
const dbRef = ref(db)
const auth = getAuth()

$$("#tab2").on("tab:show", () => {
    //put in firebase ref here
    const userUID = auth.currentUser.uid;
    const recipeListRef = ref(db, 'recipes/' + userUID);
    onValue(recipeListRef, (snapshot) => {
        if (!snapshot.exists()) {
            return
        }

        const recipes = snapshot.val()

        const oItems = recipes;
        const aKeys = Object.keys(oItems);
        $$("#groceryList").html("");
        for (let n = 0; n < aKeys.length; n++) {
            let item = oItems[aKeys[n]]
            let sCard = `
           <div class="card">
           <div class="card-content card-content-padding">
            ${item.pic ? '<img src="' + item.pic + '" alt="Picture of ' + item.title + '" />' : ''}
            <h5 class="${item.doneAt ? 'strikethrough' : ''}">${item.title}</h5>
            <p class="${item.doneAt ? 'strikethrough' : ''}">${item.ingredients}</p>
            ${item.doneAt ? '<p>Done at ' + DateTime.fromISO(item.doneAt).toLocaleString(DateTime.DATE_HUGE) + '</p>' : ''}
            </div>
            <p class="segmented segmented-raised">
                <a data-key="${aKeys[n]}" class="button deleteRecipe"><i class="icon f7-icons">delete_left</i>&nbsp;Delete
                </a>
                ${!item.doneAt ? '<button data-key="' + aKeys[n] + '" type="submit" class="button button-active markAsDone"><i class="icon f7-icons">checkmark</i>&nbsp;Mark as done</button>' : ''}
            </p>
           </div>
           `
            $$("#groceryList").append(sCard);
        }

        $$('.markAsDone').off('click')
        $$('.deleteRecipe').off('click')

        $$('.markAsDone').on('click', (e) => {
            const userId = auth.currentUser.uid,
                {key} = e.target.dataset

            get(child(dbRef, `/recipes/${userId}/${key}`)).then((snapshot) => {
                if (snapshot.exists()) {
                    const record = snapshot.val();

                    record.doneAt = new Date()

                    const updates = {
                        [`/recipes/${userId}/${key}`]: record
                    }

                    update(dbRef, updates)
                }
            }).catch((error) => {
                console.error(error);
            });
        })

        $$('.deleteRecipe').on('click', (e) => {
            const userId = auth.currentUser.uid,
                {key} = e.target.dataset

            remove(ref(db, `/recipes/${userId}/${key}`))
        })
    });
});

$$(".my-sheet").on("submit", e => {
    e.preventDefault();
    const oData = app.form.convertToData("#addItem");
    const sUser = auth.currentUser.uid;
    const sId = new Date().toISOString().replace(".", "_");

    set(ref(db, "recipes/" + sUser + "/" + sId), {
        doneAt: null,
        title: oData.title,
        ingredients: oData.ingredients,
        pic: oData.pic || null
    });

    $$('#addItem input').val('')

    app.sheet.close(".my-sheet", true);
});
